//
//  TemplateCell2.swift
//  SampleDataScroll
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Akhil. All rights reserved.
//

import UIKit

/** Class - TemplateCell2 - Subclass of UICollectionViewCell
*/
class TemplateCell2: UICollectionViewCell {
	
	@IBOutlet var labelObj: UILabel!
	@IBOutlet weak var imgobj: UIImageView!
}