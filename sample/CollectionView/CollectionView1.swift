//
//  CollectionView1.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import UIKit

/** Class - CollectionView1 - Subclass of UICollectionView
*/
class CollectionView1: UICollectionView {

	var productDetails: [Product]?
	var nib: String = "TemplateCell1"
	var cellIdentifierName: String = "CustomCell1"
	
	override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame: frame, collectionViewLayout: layout)
		
	}
	
	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
}