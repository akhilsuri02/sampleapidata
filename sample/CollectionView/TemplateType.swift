//
//  TemplateType.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import Foundation

/// Class: TemplatyType subclass of NSObject
class TemplateType: NSObject {
	var label: String?
	var imageUrl: String?
	var template: String?
	var items: [Product]?

	/// Function to initialize model class and all its instance variables.
	/// The instance variables are initialized with respective values read from dictionary object.
	/// Parameters: dataDictionary: NSDictionary
	/// Returns: Product type value - object reference of self
	func initWithData(dataDictionary: NSDictionary) -> TemplateType {
		self.label = dataDictionary.valueForKey("label") as? String
		self.imageUrl = dataDictionary.valueForKey("image") as? String
		self.template = dataDictionary.valueForKey("template") as? String
		let itemsData = dataDictionary.valueForKey("items") as? [NSDictionary]
		var itemsArray: [Product] = [Product]()
		for item: NSDictionary in itemsData! {
			let temp = Product().initWithData(item)
			itemsArray.append(temp)
		}
		self.items = itemsArray
		return self
	}
}