//
//  ViewController.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import UIKit

/** Class - ViewController Subclass of UIViewController, responsible for loading data onto screen.
It implements data source and delgates for UITableView and UICollectionView. Based on the template type, respective collection view is loaded onto screen. Template 1 and template 3 in json response is loaded by object of CollectionView1 and Template 2 is loaded by CollectionView2 object.
The class can be customized to calculate the size of cell dynamically based on the type of data that it loads.
*/
class ViewController: UIViewController,UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
	var nib: String?
	var cellIdentifierName: String?
	var dataArray: [TemplateType]?
	override func viewDidLoad() {
		super.viewDidLoad()
		let arrayFromFile1: [NSDictionary] = JSONParser.fetchDataFromJsonFile("f_one")!
		let arrayFromFile2: [NSDictionary] = JSONParser.fetchDataFromJsonFile("f_two")!
		dataArray = JSONParser.parseData(arrayFromFile1) + JSONParser.parseData(arrayFromFile2)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: TableView  Delegate & Data Source.
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
		return (dataArray?.count)!
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 150
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
		let cellIdentifier = "cellIdentifier"
		var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
		cell = nil
		if cell == nil{
			cell = UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier)
			let frame = CGRectMake(0, 0, self.view.frame.width, 150)
			cell?.frame = frame
		}
		for viewObject: UIView in cell!.contentView.subviews {
			viewObject.removeFromSuperview()
		}
		let templateData = dataArray?[indexPath.row]
		if templateData?.template == "product-template-1" || templateData?.template == "product-template-3" {
			let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
			layout.scrollDirection = .Horizontal
			let frame = CGRectMake((cell?.frame.origin.x)!, (cell?.frame.origin.y)!, tableView.frame.size.width, (cell?.frame.size.height)!)
			let collectionView = CollectionView1(frame: frame, collectionViewLayout: layout)
			nib = "TemplateCell1"
			cellIdentifierName = "CustomCell1"
			collectionView.productDetails = templateData?.items
			if templateData?.items?.count > 1 {
				
			}
			let nibNames = UINib(nibName: nib!, bundle:nil)
			collectionView.registerNib(nibNames, forCellWithReuseIdentifier: cellIdentifierName!)
			collectionView.dataSource = self
			collectionView.delegate = self
			collectionView.backgroundColor = UIColor.whiteColor()
			cell?.contentView.addSubview(collectionView)
			//			cell?.backgroundColor = UIColor.orangeColor()
		}
		else if templateData?.template == "product-template-2" {
			
			let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
			layout.scrollDirection = .Horizontal
			let frame = CGRectMake((cell?.frame.origin.x)!, (cell?.frame.origin.y)!, tableView.frame.size.width, (cell?.frame.size.height)!)
			let collectionView2 = CollectionView2(frame: frame, collectionViewLayout: layout)
			collectionView2.productDetails = templateData?.items
			nib = "TemplateCell2"
			cellIdentifierName = "CustomCell2"
			let nibNames = UINib(nibName: nib!, bundle:nil)
			collectionView2.registerNib(nibNames, forCellWithReuseIdentifier: cellIdentifierName!)
			collectionView2.dataSource = self
			collectionView2.delegate = self
			collectionView2.backgroundColor = UIColor.whiteColor()
			cell?.contentView.addSubview(collectionView2)
			//			cell?.backgroundColor = UIColor.yellowColor()
		}
		
		return cell!
	}
	
	// MARK: CollectionView Delegate & Data Source.
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	//2
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if collectionView.isKindOfClass(CollectionView1) {
			let col1: CollectionView1 = collectionView as! CollectionView1
			return (col1.productDetails?.count)!
		} else {
			let col2: CollectionView2 = collectionView as! CollectionView2
			return (col2.productDetails?.count)!
		}
	}
	
	//3
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let directoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
		print(directoryPath)
		if collectionView.isKindOfClass(CollectionView1) {
			var cell: TemplateCell1
			let col1: CollectionView1 = collectionView as! CollectionView1
			cell = col1.dequeueReusableCellWithReuseIdentifier("CustomCell1", forIndexPath: indexPath) as! TemplateCell1
			let prDetails = col1.productDetails?[indexPath.row]
			if col1.productDetails?.count > 1 {
				cell.pageCtrl.hidden = false
				cell.pageCtrl.numberOfPages = (col1.productDetails?.count)!
				cell.pageCtrl.currentPage = indexPath.row
			} else {
				cell.pageCtrl.hidden = true
			}
			cell.labelObj.text = prDetails?.labelName
			let url = NSURL(string: (prDetails?.imageUrl!)!)
			let imageName = url?.lastPathComponent
			if let image: UIImage = self.loadImage(imageName!, inDirectory: directoryPath) {
				cell.imageView.image = image
			} else {
				dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
					if let imageString: String = (prDetails?.imageUrl) {
						if let url: NSURL = NSURL(string: imageString) {
							if let data: NSData = NSData(contentsOfURL: url) {
								if let imageObject = UIImage(data: data) {
									let imageName = url.lastPathComponent
									let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
									let destinationPath = documentsPath.stringByAppendingString("/\(imageName!)")
									UIImageJPEGRepresentation(imageObject,1.0)!.writeToFile(destinationPath, atomically: true)
									cell.imageView.image = imageObject
								}
							}
						}
					}
				})
			}
			return cell
		} else {
			var cell: TemplateCell2
			let col2: CollectionView2 = collectionView as! CollectionView2
			cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomCell2", forIndexPath: indexPath) as! TemplateCell2
			let prDetails = col2.productDetails?[indexPath.row]
			cell.labelObj.text = prDetails?.labelName
			if let imageUrl: String = prDetails?.imageUrl {
				let url = NSURL(string: imageUrl)
				let imageName = url!.lastPathComponent
				if let image: UIImage = self.loadImage(imageName!, inDirectory: directoryPath) {
					cell.imgobj.image = image
				} else {
					dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
						if let imageString: String = (prDetails?.imageUrl) {
							if let url: NSURL = NSURL(string: imageString) {
								if let data: NSData = NSData(contentsOfURL: url) {
									if let imageObject = UIImage(data: data) {
										let imageName = url.lastPathComponent
										let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
										let destinationPath = documentsPath.stringByAppendingString("/\(imageName!)")
										UIImageJPEGRepresentation(imageObject,1.0)!.writeToFile(destinationPath, atomically: true)
										cell.imgobj.image = imageObject
									}
								}
							}
						}
					})
				}
			}
			
			
			return cell
		}
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		if collectionView.isKindOfClass(CollectionView1) {
			return CGSizeMake(collectionView.frame.width, 150)
		} else {
			return CGSizeMake(140, 140)
		}
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		if collectionView.isKindOfClass(CollectionView1) {
			let col1: CollectionView1 = collectionView as! CollectionView1
			let prDetails = col1.productDetails?[indexPath.row]
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			let productDetailsViewControllerObject = storyboard.instantiateViewControllerWithIdentifier("ProductDetailsViewController") as! ProductDetailsViewController
			productDetailsViewControllerObject.webUrl = prDetails?.webUrl!
			self.navigationController?.pushViewController(productDetailsViewControllerObject, animated: true)
		} else {
			let col2: CollectionView2 = collectionView as! CollectionView2
			let prDetails = col2.productDetails?[indexPath.row]
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			let productDetailsViewControllerObject = storyboard.instantiateViewControllerWithIdentifier("ProductDetailsViewController") as! ProductDetailsViewController
			productDetailsViewControllerObject.webUrl = prDetails?.webUrl!
			self.navigationController?.pushViewController(productDetailsViewControllerObject, animated: true)
		}
	}
	
	// MARK: Utility
 /// Function to load image file from document directory path and return UIImage object.
	/// Paramters: fileName - name of file
	/// directoryPath - path of the file
 /// Returns: Object of UIImage
	func loadImage(fileName: String, inDirectory directoryPath: String) -> UIImage? {
		if let result: UIImage = UIImage(contentsOfFile:"\(directoryPath)/\(fileName)") {
			return result
		}
		return nil
	}
}
