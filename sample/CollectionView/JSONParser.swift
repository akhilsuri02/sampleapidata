//
//  JSONParser.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import Foundation
/* Class - JSONParser - Subclass of NSObject, class has static methods that are used to read the data from JSON files present in the resource directory and load data from same. Once the data is loaded, it is parsed by a function into objects of respective model data.
*/

class JSONParser: NSObject {
	/// Function to read the JSON file from document directory and load it's content into an objec of NSArray.
	/// Parameters: fileName - of type String, it is the name of the files that has to be read from document directory.
	/// Returns: [NSDictionary] - array, it returns the array of NSDictionaries having data read from json file.
	static func fetchDataFromJsonFile(fileName: String) -> [NSDictionary]? {
		let filePath = NSBundle.mainBundle().pathForResource(fileName, ofType: "json")
		let data = NSData(contentsOfFile: filePath!)
		do {
			let array: [NSDictionary] = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [NSDictionary]
			return array
			// success ...
		} catch {
			// failure
			print("Fetch failed: \((error as NSError).localizedDescription)")
		}
		return nil
	}
	/// Function to parse the data from nsdictionary object into respective model class objects.
	/// Parameters: dataArray - array of NSDictionaries, it is the data read from json files, which was parsed into objects of NSDictionary.
	/// Returns: [TemplateType] - array, it returns the array of TemplateType model class having data read from json file.
	static func parseData(dataArray: [NSDictionary]) -> [TemplateType] {
		var parsedArray: [TemplateType] = [TemplateType]()
		for dataItem: NSDictionary in dataArray {
			print(dataItem)
			let temp = TemplateType().initWithData(dataItem)
			parsedArray.append(temp)
		}
		return parsedArray
	}
}