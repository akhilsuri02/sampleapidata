//
//  ProductDetailsViewController.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import Foundation
import UIKit
/* Class - ProductDetailsViewController - Subclass of UIViewController, class responsible for loading content into webpage. The class has a webUrl instance variable, which is set when the class is initialized. The method loadWebView is then responsible for loading webpage.
*/
class ProductDetailsViewController: UIViewController, UIWebViewDelegate {
	@IBOutlet var webViewObject: UIWebView!
	var webUrl: String?
	override func viewDidLoad() {
		super.viewDidLoad()
		self.loadWebView(webUrl!)
	}
	/// Function responsible for loading webpage in the form of NSURLRequest and then forwarding it to object of UIWebView.
	/// Parameters: webUrl of type String, accepts the link of webpage that needs to be loaded into UIWebView
	func loadWebView(webUrl: String) {
		let url = NSURL(string: webUrl)
		let request = NSURLRequest(URL: url!)
		self.webViewObject.loadRequest(request)
	}
}