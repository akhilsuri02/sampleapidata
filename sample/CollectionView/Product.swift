//
//  Product.swift
//  CollectionView
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

import Foundation

/// Class: Product subclass of NSObject
class Product: NSObject {
	var labelName: String?
	var imageUrl: String?
	var webUrl: String?
	
	/// Function to initialize model class and all its instance variables.
	/// The instance variables are initialized with respective values read from dictionary object.
	/// Parameters: dataDictionary: NSDictionary
	/// Returns: Product type value - object reference of self
	func initWithData(dataDictionary: NSDictionary) -> Product {
		self.labelName = dataDictionary.valueForKey("label") as? String
		if let imageName = dataDictionary.valueForKey("image") as? String {
			self.imageUrl = imageName
		}
		if let stringName = dataDictionary.valueForKey("image_url") as? String {
			self.imageUrl = stringName
		}
		self.webUrl = dataDictionary.valueForKey("web-url") as? String
		return self
	}
}