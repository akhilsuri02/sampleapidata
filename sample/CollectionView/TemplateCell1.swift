//
//  TemplateCell1.swift
//  SampleDataScroll
//
//  Created by Akhil-new on 07/02/16.
//  Copyright © 2016 Akhil. All rights reserved.
//

import UIKit

/** Class - TemplateCell1 - Subclass of UICollectionViewCell
*/
class TemplateCell1: UICollectionViewCell {
	
	@IBOutlet var labelObj: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet var pageCtrl: UIPageControl!
}